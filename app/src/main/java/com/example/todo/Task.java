package com.example.todo;

import android.widget.EditText;

public class Task {
    String texttodo;
    boolean isSelected;

    public Task(String texttodo, boolean isSelected) {
        this.texttodo = texttodo;
        this.isSelected = isSelected;
    }



    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public Task(boolean isSelected) {
        this.isSelected = isSelected;
    }

    Task(String texttodo) {
        this.texttodo = texttodo;
    }

    String getTexttodo() {
        return texttodo;
    }

    public void setTexttodo(String texttodo) {
        this.texttodo = texttodo;
    }
}

